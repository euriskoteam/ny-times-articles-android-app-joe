# NY Times News

A simple master/details app that fetches and display news from NY Times API.

## Project Setup

Clone the project from the Git repository and apply the following:
- Sync Gradle
- Clean Project
- Rebuild Project

### IDE Requirements

[Android Studio](https://developer.android.com/studio/)

### Installation

Connect an Android device or start an emulator and run the app.

## Running the instrumented test

Right click the IntrumentedEspressoTest.java and click run "IntrumentedEspressoTest"

### Instrumented test description

The test runs as follows:

 - Swipe refresh to get the news list
 - News item click
 - Go to details screen
 - Press back return to main screen.
 - Repeat above and stop on the third news item

### Generating a coverage report

To get a coverage report, execute the below command in Terminal window of Android Studio:

```
./gradlew createDebugCoverageReport
```

The report will be located at the following path:
app/build/outputs/reports/coverage/debug/

## Developed By

* **Joe Abou Assi**
