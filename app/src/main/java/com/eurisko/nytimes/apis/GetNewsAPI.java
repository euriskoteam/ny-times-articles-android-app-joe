package com.eurisko.nytimes.apis;

import android.app.Activity;
import android.os.AsyncTask;

import com.eurisko.nytimes.R;
import com.eurisko.nytimes.models.NewsModel;
import com.eurisko.nytimes.utils.HttpResponse;
import com.eurisko.nytimes.utils.GlobalFunctions;
import com.eurisko.nytimes.utils.GlobalVars;
import com.eurisko.nytimes.utils.NetworkUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class GetNewsAPI extends AsyncTask<Void, Void, Void> {

    Activity activity;
    OnNewsListener onNewsListener;
    HttpResponse httpResponse;
    String section, period;
    boolean hasInternet, didFail = true;
    public boolean isRunning = false;
    String msg = "";
    ArrayList<NewsModel> newsList;

    public GetNewsAPI(Activity activity, String section, String period, OnNewsListener onNewsListener) {
        this.activity = activity;
        this.onNewsListener = onNewsListener;
        this.section = section;
        this.period = period;
        msg = activity.getString(R.string.error_msg);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        isRunning = true;
        hasInternet = NetworkUtils.CheckNetwork(activity);
        if (!hasInternet) {
            msg = activity.getString(R.string.no_internet_msg);
        }
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            if (hasInternet) {
                HashMap<String, String> headers = new HashMap<>();

                httpResponse = NetworkUtils.apiGet(GlobalVars.getNyTimesApi(section, period), headers);

                if (httpResponse.getCode() == 200) {
                    JSONObject jsonObject = new JSONObject(httpResponse.getResponse());
                    String status = "";
                    if (jsonObject.has("status") && !jsonObject.isNull("status")) {
                        status = jsonObject.getString("status");
                    }
                    if (status.equals("OK")) {
                        if (jsonObject.has("results") && !jsonObject.isNull("results")) {
                            JSONArray results = jsonObject.getJSONArray("results");
                            newsList = new ArrayList<>();
                            for (int i = 0; i < results.length(); i++) {
                                newsList.add(NewsModel.fromJson(results.getJSONObject(i)));
                            }
                            didFail = false;
                        }
                    }
                }
            }
        } catch (Exception e) {
            GlobalFunctions.printException(e);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void o) {
        super.onPostExecute(o);
        try {
            isRunning = false;
            if (onNewsListener != null) {
                if (didFail) {
                    onNewsListener.onFail(msg);
                } else {
                    onNewsListener.onNewsReceived(newsList);
                }
            }
        } catch (Exception e) {
            GlobalFunctions.printException(e);
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        isRunning = false;
    }

    @Override
    protected void onCancelled(Void o) {
        super.onCancelled(o);
        isRunning = false;
    }

    public interface OnNewsListener {
        void onNewsReceived(ArrayList<NewsModel> newsList);

        void onFail(String msg);
    }
}
