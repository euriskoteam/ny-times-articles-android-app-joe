package com.eurisko.nytimes.activities;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.eurisko.nytimes.R;
import com.eurisko.nytimes.adapters.NewsAdapter;
import com.eurisko.nytimes.apis.GetNewsAPI;
import com.eurisko.nytimes.models.NewsModel;
import com.eurisko.nytimes.utils.GlobalFunctions;
import com.eurisko.nytimes.utils.GlobalVars;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    SwipeRefreshLayout srlNews;
    RecyclerView rvNews;
    TextView tvNoNews;
    GetNewsAPI getNewsAPI;
    NewsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupView();
        implementView();
        getNews(false);
    }

    private void setupView() {
        srlNews = findViewById(R.id.srlNews);
        rvNews = findViewById(R.id.rvNews);
        tvNoNews = findViewById(R.id.tvNoNews);
    }

    private void implementView() {
        rvNews.setAdapter(new NewsAdapter(this, new ArrayList<NewsModel>()));
        srlNews.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getNews(true);
            }
        });
    }

    private void getNews(boolean isRefresh) {
        if (!isRefresh) {
            srlNews.setRefreshing(true);
        }
        tvNoNews.setVisibility(View.GONE);
        getNewsAPI = new GetNewsAPI(this, GlobalVars.ALL_SECTIONS, GlobalVars.PERIOD_7, new GetNewsAPI.OnNewsListener() {
            @Override
            public void onNewsReceived(ArrayList<NewsModel> newsList) {
                try {
                    srlNews.setRefreshing(false);
                    tvNoNews.setVisibility(View.GONE);
                    handleNews(newsList);
                } catch (Exception e) {
                    GlobalFunctions.printException(e);
                }
            }

            @Override
            public void onFail(String msg) {
                try {
                    handleEmptyNews();
                    GlobalFunctions.showToast(MainActivity.this, msg);
                } catch (Exception e) {
                    GlobalFunctions.printException(e);
                }
            }
        });
        getNewsAPI.execute();

    }

    private void handleEmptyNews() {
        srlNews.setRefreshing(false);
        if (rvNews.getAdapter() == null || rvNews.getAdapter().getItemCount() == 0) {
            tvNoNews.setVisibility(View.VISIBLE);
        }
    }

    private void handleNews(ArrayList<NewsModel> newsList) {
        if (newsList.isEmpty()) {
            tvNoNews.setVisibility(View.VISIBLE);
        }
        adapter = new NewsAdapter(this, newsList);
        rvNews.setAdapter(adapter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (getNewsAPI != null) {
            getNewsAPI.cancel(true);
        }
    }
}
