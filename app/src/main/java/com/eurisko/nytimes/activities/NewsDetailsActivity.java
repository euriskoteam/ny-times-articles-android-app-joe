package com.eurisko.nytimes.activities;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.eurisko.nytimes.R;
import com.eurisko.nytimes.models.NewsModel;
import com.eurisko.nytimes.utils.GlobalFunctions;
import com.eurisko.nytimes.utils.GlobalVars;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class NewsDetailsActivity extends AppCompatActivity {

    TextView tvNewsTitle, tvNewsDate, tvNewsBy, tvNewsDescription;
    ImageView ivNews;
    ImageLoader loader;
    DisplayImageOptions options;

    NewsModel newsModel;

    int screenWidth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);

        handleBundles();
        setupView();
        implementView();
    }

    private void handleBundles() {
        try {
            newsModel = (NewsModel) getIntent().getSerializableExtra(GlobalVars.NEWS_BEAN_BUNDLE);
        } catch (Exception e) {
            GlobalFunctions.printException(e);
        }
        if (newsModel == null) {
            newsModel = new NewsModel();
        }
    }

    private void setupView() {
        screenWidth = GlobalFunctions.getScreenWidth(this);
        loader = GlobalFunctions.getImageLoader(this);
        options = GlobalFunctions.getImageOptions(R.drawable.default_pic);
        tvNewsTitle = findViewById(R.id.tvNewsTitle);
        tvNewsDate = findViewById(R.id.tvNewsDate);
        tvNewsBy = findViewById(R.id.tvNewsBy);
        tvNewsDescription = findViewById(R.id.tvNewsDescription);
        ivNews = findViewById(R.id.ivNews);
    }

    private void implementView() {
        tvNewsTitle.setText(newsModel.getTitle());
        tvNewsBy.setText(newsModel.getBy());
        tvNewsDate.setText(newsModel.getDate());
        tvNewsDescription.setText(newsModel.getDescription());

        loader.displayImage(newsModel.getImage(), ivNews, options, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                try {
                    ivNews.getLayoutParams().height = screenWidth * loadedImage.getHeight() / loadedImage.getWidth();
                } catch (Exception e) {
                    GlobalFunctions.printException(e);
                }
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        });
    }
}
