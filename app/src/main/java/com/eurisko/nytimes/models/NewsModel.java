package com.eurisko.nytimes.models;

import com.eurisko.nytimes.utils.GlobalFunctions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;

public class NewsModel implements Serializable {
    private String title = "";
    private String description = "";
    private String date = "";
    private String by = "";
    private String image = "";
    private String thumbnail = "";

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getBy() {
        return by;
    }

    public void setBy(String by) {
        this.by = by;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public static NewsModel fromJson(JSONObject jsonObject) {
        NewsModel newsModel = new NewsModel();
        try {
            if (jsonObject.has("title") && !jsonObject.isNull("title")) {
                newsModel.setTitle(jsonObject.getString("title"));
            }
            if (jsonObject.has("abstract") && !jsonObject.isNull("abstract")) {
                newsModel.setDescription(jsonObject.getString("abstract"));
            }
            if (jsonObject.has("byline") && !jsonObject.isNull("byline")) {
                newsModel.setBy(jsonObject.getString("byline"));
            }
            if (jsonObject.has("published_date") && !jsonObject.isNull("published_date")) {
                newsModel.setDate(jsonObject.getString("published_date"));
            }
            if (jsonObject.has("media") && !jsonObject.isNull("media")) {
                JSONArray mediaArr = jsonObject.getJSONArray("media");
                if (mediaArr.length() > 0) {
                    JSONObject mediaJson = mediaArr.getJSONObject(0);
                    if (mediaJson.has("type") && !mediaJson.isNull("type")) {
                        String type = mediaJson.getString("type");
                        if (type.equals("image")) {
                            if (mediaJson.has("media-metadata") && !mediaJson.isNull("media-metadata")) {
                                JSONArray mediaMetaDataArr = mediaJson.getJSONArray("media-metadata");
                                for (int k = 0; k < mediaMetaDataArr.length(); k++) {
                                    JSONObject mediaMetaDataJson = mediaMetaDataArr.getJSONObject(k);
                                    String url = "";
                                    if (mediaMetaDataJson.has("url") && !mediaMetaDataJson.isNull("url")) {
                                        url = mediaMetaDataJson.getString("url");
                                    }
                                    if (mediaMetaDataJson.has("format") && !mediaMetaDataJson.isNull("format")) {
                                        String format = mediaMetaDataJson.getString("format");
                                        if (format.equals("Large Thumbnail")) {
                                            newsModel.setThumbnail(url);
                                        } else if (format.equals("mediumThreeByTwo440")) {
                                            newsModel.setImage(url);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            GlobalFunctions.printException(e);
        }

        return newsModel;
    }
}
