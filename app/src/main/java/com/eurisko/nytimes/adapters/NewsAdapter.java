package com.eurisko.nytimes.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eurisko.nytimes.R;
import com.eurisko.nytimes.activities.NewsDetailsActivity;
import com.eurisko.nytimes.models.NewsModel;
import com.eurisko.nytimes.customviews.CircleImageView;
import com.eurisko.nytimes.utils.GlobalFunctions;
import com.eurisko.nytimes.utils.GlobalVars;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsHolder> {

    Activity activity;
    ArrayList<NewsModel> newsList;
    LayoutInflater inflater;
    ImageLoader loader;
    DisplayImageOptions options;

    public NewsAdapter(Activity activity, ArrayList<NewsModel> newsList) {
        this.activity = activity;
        this.newsList = newsList;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loader = GlobalFunctions.getImageLoader(activity);
        options = GlobalFunctions.getImageOptions(R.drawable.default_pic);
    }

    @NonNull
    @Override
    public NewsHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = inflater.inflate(R.layout.news_row, viewGroup, false);
        return new NewsHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsHolder newsHolder, int i) {
        NewsModel newsModel = newsList.get(i);

        newsHolder.tvNewsTitle.setText(newsModel.getTitle());
        newsHolder.tvNewsBy.setText(newsModel.getBy());
        newsHolder.tvNewsDate.setText(newsModel.getDate());

        loader.displayImage(newsModel.getThumbnail(), newsHolder.civNews, options);

        newsHolder.rlNews.setTag(i);
        newsHolder.rlNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    int pos = (int) view.getTag();
                    NewsModel news = newsList.get(pos);
                    Intent intent = new Intent(activity, NewsDetailsActivity.class);
                    intent.putExtra(GlobalVars.NEWS_BEAN_BUNDLE, news);
                    activity.startActivity(intent);
                } catch (Exception e) {
                    GlobalFunctions.printException(e);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return newsList == null ? 0 : newsList.size();
    }

    public static class NewsHolder extends RecyclerView.ViewHolder {

        RelativeLayout rlNews;
        TextView tvNewsTitle, tvNewsDate, tvNewsBy;
        CircleImageView civNews;

        public NewsHolder(@NonNull View itemView) {
            super(itemView);
            rlNews = itemView.findViewById(R.id.rlNews);
            tvNewsTitle = itemView.findViewById(R.id.tvNewsTitle);
            tvNewsDate = itemView.findViewById(R.id.tvNewsDate);
            tvNewsBy = itemView.findViewById(R.id.tvNewsBy);
            civNews = itemView.findViewById(R.id.civNews);
        }
    }

}
