package com.eurisko.nytimes.utils;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class NetworkUtils {

    public static boolean CheckNetwork(Context context) {
        try {
            ConnectivityManager connec = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                Network[] networks = connec.getAllNetworks();
                for (Network network : networks) {
                    NetworkInfo netInfo = connec.getNetworkInfo(network);
                    if ((netInfo.getType() == ConnectivityManager.TYPE_WIFI || netInfo.getType() == ConnectivityManager.TYPE_MOBILE) && netInfo.isConnected()) {
                        return true;
                    }
                }
            } else {
                NetworkInfo wifi = connec.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                NetworkInfo mobile = connec.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
                if ((wifi != null && wifi.isConnected()) || (mobile != null && mobile.isConnected()))
                    return true;
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();

            return false;
        }
    }

    public static HttpResponse apiGet(String urlStr, HashMap<String, String> headers) throws Exception {
        InputStream inputStream;
        String result = "";
        HttpResponse response = new HttpResponse();

        URL url = new URL(urlStr);

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setReadTimeout(GlobalVars.NETWORK_TIME_OUT);
        connection.setConnectTimeout(GlobalVars.NETWORK_TIME_OUT);
        connection.setRequestMethod("GET");
        connection.setDoInput(true);

        if (headers != null) {
            Iterator it = headers.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<String, String> pair = (Map.Entry) it.next();
                connection.setRequestProperty(pair.getKey(), pair.getValue());
                it.remove();
            }
        }

        connection.connect();

        if (connection.getResponseCode() < HttpURLConnection.HTTP_BAD_REQUEST)
            inputStream = connection.getInputStream();
        else
            inputStream = connection.getErrorStream();

        if (inputStream != null) {
            result = PrintStream(inputStream);
            inputStream.close();
        }

        response.setCode(connection.getResponseCode());
        response.setResponse(result);

        return response;
    }


    public static String PrintStream(InputStream in) throws Exception {
        InputStreamReader isr = new InputStreamReader(in, Charset.forName("UTF-8"));
        BufferedReader buffer = new BufferedReader(isr);
        String str = "";
        StringBuilder strbuild = new StringBuilder();
        while ((str = buffer.readLine()) != null) {
            strbuild.append(str + "\n");
        }
        return strbuild.toString();

    }

}
