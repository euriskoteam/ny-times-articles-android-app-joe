package com.eurisko.nytimes.utils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.Toast;

import com.eurisko.nytimes.BuildConfig;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class GlobalFunctions {


    private static final String LOG_TAG = "NY_TIMES";

    public static void showToast(Activity activity, String Message) {
        Toast.makeText(activity, Message, Toast.LENGTH_SHORT).show();
    }

    public static int getScreenWidth(Activity a) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        a.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;

    }

    public static void printException(Exception e) {
        if (BuildConfig.DEBUG) {
            if (e != null) {
                Log.d(LOG_TAG, Log.getStackTraceString(e));
            }
        }
    }

    public static ImageLoader getImageLoader(Activity activity) {
        ImageLoader imageLoader = ImageLoader.getInstance();
        if (!imageLoader.isInited()) {
            imageLoader.init(ImageLoaderConfiguration.createDefault(activity));
        }
        return imageLoader;
    }

    public static DisplayImageOptions getImageOptions(int defaultIcon) {
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .showImageOnLoading(defaultIcon)
                .showImageOnFail(defaultIcon)
                .showImageForEmptyUri(defaultIcon)
                .build();
        return options;
    }
}
