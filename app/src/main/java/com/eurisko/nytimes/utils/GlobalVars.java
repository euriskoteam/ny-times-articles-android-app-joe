package com.eurisko.nytimes.utils;

public class GlobalVars {
    /*
     *
     * Http values
     *
     * */
    public static final int NETWORK_TIME_OUT = 20000;

    /*
     *
     * API Keys
     *
     * */
    public static final String NY_TIMES_KEY = "Rk6GfVjL9XA3A5ipo7bjr2fNh80CpeA5";

    /*
     *
     * API Params
     *
     * */
    public static final String ALL_SECTIONS = "all-sections";
    public static final String PERIOD_7 = "7";

    /*
     *
     * API Endpoints
     *
     * */

    public static final String MAIN_URL = "http://api.nytimes.com/";

    public static String getNyTimesApi(String section, String period) {
        return MAIN_URL + "/svc/mostpopular/v2/mostviewed/" + section + "/" + period + ".json?api-key=" + GlobalVars.NY_TIMES_KEY;
    }

    /*
     *
     * Bundle Tags
     *
     * */
    public static final String NEWS_BEAN_BUNDLE = "news_bean_bundle";

}
