package com.eurisko.nytimes.utils;


import java.io.Serializable;

public class HttpResponse implements Serializable {
    String response = "";
    int code;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
