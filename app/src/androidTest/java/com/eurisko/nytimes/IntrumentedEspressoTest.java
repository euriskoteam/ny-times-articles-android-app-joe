package com.eurisko.nytimes;

import android.app.Activity;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;

import com.eurisko.nytimes.activities.MainActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;


@RunWith(AndroidJUnit4.class)
@LargeTest
public class IntrumentedEspressoTest {


    private int count = 0;
    Activity activity;
    RecyclerView rvNews;


    @Before
    public void setup() {
        this.activity = activityTestRule.getActivity();
        this.rvNews = activity.findViewById(R.id.rvNews);
    }

    @Rule
    public ActivityTestRule<MainActivity> activityTestRule =
            new ActivityTestRule<>(MainActivity.class);


    @Test
    public void NewsItemsClickTest() {
        Espresso.onView(ViewMatchers.withId(R.id.srlNews))
                .perform(ViewActions.swipeDown());

        this.activity = activityTestRule.getActivity();

        this.rvNews = activity.findViewById(R.id.rvNews);
        if (rvNews.getAdapter() != null) {
            this.count = rvNews.getAdapter().getItemCount();
            for (int i = 0; i < count; i++) {
                Espresso.onView(ViewMatchers.withId(R.id.rvNews))
                        .perform(RecyclerViewActions.actionOnItemAtPosition(i, ViewActions.click()));
                Espresso.pressBack();
                Espresso.onView(ViewMatchers.withId(R.id.srlNews))
                        .perform(ViewActions.swipeDown());
                if (i == 2) {
                    break;
                }
            }
        }
    }
}